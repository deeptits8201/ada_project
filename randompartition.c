#include"header.h"

int fnRandomPartition(int arr[], int l, int r) 
{ 
	int n = r-l+1; 
	int pivot = rand() % n; 
	int temp = arr[l+pivot];
	arr[l+pivot] = arr[r];
	arr[r] = temp; 
	return partition(arr, l, r); 
} 