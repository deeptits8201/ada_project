#include"header.h" 

int fnRandomSelect(int arr[], int l, int r, int k) 
{ 
	if (k > 0 && k <= r - l + 1) 
	{ 

		int q = fnRandomPartition(arr, l, r); 

		if (q-l == k-1) 
			return arr[q]; 
		if (q-l > k-1)
			return fnRandomSelect(arr, l, q-1, k); 

		return fnRandomSelect(arr, q+1, r, k-q+l-1); 
	} 
 
	return -1; 
}